jQuery(document).ready(function ($) {
    
    var answers = new Answers;
    
    $('.send-order input[type=submit]').click(function (event) {
        event.preventDefault();
        
        var send_data ={ action: 'setOrder' };
        var form_data = $('form.send-order').serializeArray();
        
        form_data.forEach(function (item) {
            send_data[item.name] = item.value;
        });

        jQuery.post(shopCodediller.ajaxurl,send_data, function (response) {
            $('.single-product-popup').fadeOut();
            
            var answer = answers.aboutAnswers(response);
            $('.ajax-answer .description-error').append(answer.name_answer);
            $('.ajax-answer').css({
                border: '1px solid ' + answer.color
            });
            setTimeout(function () {
                $('.ajax-answer').fadeIn();
            },600);
        })
        
    });
});
