
// Shop CodeDiller script
jQuery(function( $ ) {
    
    $('a.open-popup').click(function () {
        $('.single-product-popup').fadeIn();
    });
    
    $('.button-close').click(function () {
        $('.single-product-popup, .ajax-answer').fadeOut();
    });
});