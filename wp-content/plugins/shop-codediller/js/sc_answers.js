function Answers() {
    this.aboutAnswers = function (number_answer) {

        // Colors
        var red     = '##c0392b',
            green   = '#27ae60',
            yellow  = '#f39c12';

        /*
         * 1000 - start Ajax answers
         */
        var answers = {
            0: { color: red, name_answer: 'Error' },
            1: { color: green, name_answer: 'Ok' },
            1000: { color: red, name_answer: 'Error'},
            1001:{ color: green, name_answer: 'Ok'},
            1002:{ color: yellow, name_answer: 'Field(s) is empty'},
            1003:{ color: red, name_answer: 'Order not sent, please resubmit'},
            1004:{ color: green, name_answer: 'Order sent successfully'}
        };

        return answers[number_answer];
    }
}