<?php
    class SC_Init {
        
        public static $product;
        public static $order;
        public static $role;
        public static $init;
        
        public function __construct(){
            add_action( 'wp_enqueue_scripts', array( $this, 'registerStyle' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'registerScriptInHeader' ));
            add_action( 'wp_footer', array( $this, 'registerScript' ) );
            add_action( 'template_include', array( $this, 'loadTemplates' ) );
            self::registerAjax();
        }

        static function setCustomPostTypes(){
            self::$product = new SC_Product;
            self::$order = new SC_Order;
            
        }
        
        public static function init(){
            self::setCustomPostTypes();
            self::$role = new SC_Roles;
            self::$init = new SC_Init;
        }

        /**
         * Register new taxonomy to post type by name
         *
         * @param string $singular
         * @param string $plural
         * @param string $name_taxonomy
         * @param array $name_post_type
         */
        static function registerTaxonomy ($singular, $plural, $name_taxonomy, $name_post_type ){
            register_taxonomy( $name_taxonomy, $name_post_type, array(
                'labels'                => array(
                    'name'              => $plural,
                    'singular_name'     => $singular,
                    'all_items'         => 'All ' . $plural,
                    'edit_item'         => 'Edit ' . $singular,
                    'update_item'       => 'Update ' . $singular,
                    'add_new_item'      => 'Add New '. $singular,
                    'new_item_name'     => 'New ' . $singular . ' name',
                    'menu_name'         => $singular,
                ),
                'public'                => true,
                'show_in_nav_menus'     => true,
                'show_ui'               => true,
                'update_count_callback' => '',
                'capabilities'          => array(),
                'show_admin_column'     => false,
            ) );
        }


        /**
         * Register new term to taxonomy
         * 
         * @param $term_name
         * @param $taxonomy_name
         * @param $description
         * @param $slug
         */
        static function registerTermTaxonomy($term_name, $taxonomy_name, $description, $slug ){
            wp_insert_term(
                $term_name,
                $taxonomy_name,
                array(
                    'description' => $description,
                    'slug'        => $slug,
                )
            );
        }


        /**
         * Start after activation plugin
         */
        public static function onActivation(){
            SC_Roles::setRoles();
        }


        /**
         * Start after deactivation plugin
         */
        public static function onDeactivation(){
            SC_Roles::removeRoles();
        }


        /**
         * Start after uninstall plugin
         */
        public static function onUninstall(){
            SC_Order::removeTermTaxonomy();
        }
        
        /**
         * Register CSS 
         */
        public function registerStyle(){
            wp_enqueue_style( 'sc_style', SHOP_CODDILER__PLUGIN_URL . 'css/sc_style.css' );
        }
        
        /**
         * Register  JS script
         */
        public function registerScript(){
            wp_enqueue_script( 'sc_script', SHOP_CODDILER__PLUGIN_URL . 'js/sc_script.js');
        }
        
        public function registerScriptInHeader(){
            wp_enqueue_script( 'sc_answers', SHOP_CODDILER__PLUGIN_URL . 'js/sc_answers.js', array('jquery') );
        }
        
        /**
         * Register new templates for product page and product archive page
         * 
         * @param $template
         * @return string
         */
        public function loadTemplates($template){
            if ( get_query_var( 'post_type' ) !== 'product' ) {
                return $template;
            }

            if ( is_archive() || is_search() ) {
                if ( file_exists( get_stylesheet_directory(). '/archive-product.php' ) ) {
                    return get_stylesheet_directory() . '/archive-product.php';
                } else {
                    return SHOP_CODDILER__PLUGIN_DIR . 'templates/archive-product.php';
                }
            } elseif(is_singular('product')) {
                
                add_action( 'wp_enqueue_scripts', array( 'SC_Buy', 'registerScriptForAjax' ), 99 );
                
                if (  file_exists( get_stylesheet_directory(). '/single-product.php' ) ) {
                    return get_stylesheet_directory() . '/single-product.php';
                } else {
                    return SHOP_CODDILER__PLUGIN_DIR . 'templates/single-product.php';
                }
            }else{
                return $template;
            }

            return $template;
        }


        /**
         *  Register ajax actions
         */
        static function registerAjax(){
            // For buy form in single-product page
            add_action( 'wp_ajax_nopriv_setOrder', array( 'SC_Buy', 'setOrderByAjax') );
            add_action( 'wp_ajax_setOrder', array( 'SC_Buy', 'setOrderByAjax') );
        }
    }