<?php
    
    Class SC_Roles{
        
        function __construct(){
            add_filter('pre_get_posts', array( $this, 'postsForCurrentAuthor' ) );
        }
        
        public function postsForCurrentAuthor($query) {
            global $pagenow;

            if( 'edit.php' != $pagenow || !$query->is_admin )
                return $query;

            if( !current_user_can( 'edit_others_posts' ) ) {
                global $user_ID;
                $query->set('author', $user_ID );
            }
            return $query;
        }


        /**
         * Add new privilege to administrator
         */
        static function addPrivilegeToAdministrator(){
            
            $role = get_role( 'administrator' );
            
            $role->add_cap( 'edit_product' );
            $role->add_cap( 'read_product' );
            $role->add_cap( 'delete_product' );
            $role->add_cap( 'edit_products' );
            $role->add_cap( 'edit_others_products' );
            $role->add_cap( 'publish_products' );
            $role->add_cap( 'read_private_products' );
            $role->add_cap( 'delete_products' );
            $role->add_cap( 'delete_private_products' );
            $role->add_cap( 'delete_published_products' );
            $role->add_cap( 'delete_others_products' );
            $role->add_cap( 'edit_published_products' );
            $role->add_cap( 'edit_products' );
            
            $role->add_cap( 'edit_booking' ); 
            $role->add_cap( 'read_booking' ); 
            $role->add_cap( 'delete_booking' ); 
            $role->add_cap( 'edit_bookings' ); 
            $role->add_cap( 'edit_others_bookings' ); 
            $role->add_cap( 'publish_bookings' ); 
            $role->add_cap( 'read_private_bookings' );
            $role->add_cap( 'delete_bookings' ); 
            $role->add_cap( 'delete_private_bookings' ); 
            $role->add_cap( 'delete_published_bookings' ); 
            $role->add_cap( 'delete_others_bookings' ); 
            $role->add_cap( 'edit_private_bookings' ); 
            $role->add_cap( 'edit_published_bookings' ); 
            $role->add_cap( 'edit_bookings' ); 
        }

        /**
         * Remove privilege to administrator
         */
        static function removePrivilegeToAdministrator(){
            // remove privilege for administrator
        }

        /**
         * Add new role Seller
         */
        static function addRoleSeller(){
            add_role( 'seller', 'Seller',
                array(
                    'read'                      => true,
                    'read_product'              => true,
                    'read_products'             => true,
                    'publish_products'          =>true,
                    'delete_private_products'   => true,
                    'delete_published_products' => true,
                    'read_private_products'     => true,
                    'edit_private_products'     => true,
                    'edit_published_products'     => true,
                    'edit_products'             => true,
                    'edit_product'             => true,
                    'upload_files'             => true,
//                    'manage_categories'         => true,
                ) 
            );
        }

        /**
         * Add new role Buyer
         */
        static function addRoleBuyer(){
            add_role( 'buyer', 'Buyer',
                array(
                    'read'  => true,
                )
            );
        }
        
        /**
         * Add roles after activation plugin
         */
        static function setRoles(){
            self::addRoleBuyer();
            self::addRoleSeller();
            self::addPrivilegeToAdministrator();
        }
        
        static function removeRoles (){
            remove_role( 'seller' );
            remove_role( 'buyer' );
        }
        
    }
   
    
    