<?php
    
    Class SC_PostType{
        
        public function __construct() {
            add_action( 'init', array( $this, 'sc_register_post_type' ) );
            add_action( 'init', array( $this, 'sc_register_taxonomy') );
        }

        public function register_post_type(){
            $args_orders = array(
                'public'            => true,
                'labels'            => [
                    'name'              => _('Orders'),
                    'singular_name'     => _('Order'),
                ],
                'has_archive'       => true,
                'rewrite'           => [
                    'slug'              => 'orders',
                ],
                'capability_type'   => 'post',
                'menu_icon'         => 'dashicons-chart-area',
                'capabilities'      => array(
                    'create_posts'      => 'do_not_allow',
                ),
                'map_meta_cap' => true,
            );
            $args_products = array(
                'public'        => true,
                'labels'        => [
                    'name'          => _('Products'),
                    'singular_name' => _('Product'),
                ],
                'has_archive'   => true,
                'rewrite'       => [
                    'slug'          => 'products',
                ],
                'capability_type'   => 'post',
                'taxonomies'        => array( 'category', 'post_tag' ),
                'supports'          => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
                'menu_icon'         => 'dashicons-store',
            );

            register_post_type( 'orders', $args_orders );
            register_post_type( 'products', $args_products );
        }
        
        public function register_taxonomy(){
            register_taxonomy( 'delivery_method', array('orders'), array(
                'labels'                => array(
                    'name'              => 'Delivery methods',
                    'singular_name'     => 'Delivery method',
                    'all_items'         => 'All Delivery methods',
                    'edit_item'         => 'Edit Delivery method',
                    'update_item'       => 'Update Delivery method',
                    'add_new_item'      => 'Add New Delivery method',
                    'new_item_name'     => 'New Delivery method Name',
                    'menu_name'         => 'Delivery method',
                ),
                'public'                => true,
                'show_in_nav_menus'     => true,
                'show_ui'               => true,
                'update_count_callback' => '',
                'capabilities'          => array(),
                'show_admin_column'     => false,
            ) );

            register_taxonomy( 'Order Status', array('orders'), array(
                'labels'                => array(
                    'name'              => 'Orders Status',
                    'singular_name'     => 'Order Status',
                    'all_items'         => 'All Orders Status',
                    'edit_item'         => 'Edit Order Status',
                    'update_item'       => 'Update Order Status',
                    'add_new_item'      => 'Add New Order Status',
                    'new_item_name'     => 'New Order Status Name',
                    'menu_name'         => 'Order Status',
                ),
                'public'                => true,
                'show_in_nav_menus'     => true,
                'show_ui'               => true,
                'update_count_callback' => '',
                'capabilities'          => array(),
                'show_admin_column'     => false,
            ));
        }
    }