<?php
    class SC_Order{

        public $singular = 'Order';
        public $plural = 'Orders';

        public function __construct(){
            
            add_action( 'init', array( $this, 'newPostType' ) );
            add_action( 'init', array( $this, 'registerTaxonomyDeliveryMethod' ) );
            add_action( 'init', array( $this, 'registerTaxonomyOrderStatus' ) );
            add_action( 'init', array( $this, 'setTermTaxonomy' ) );
            
        }

        /**
         *  Register new post type
         */
        public function newPostType(){

            $capabilities = array(
            );

            $labels = array(
                'name'                  => $this->plural,
                'singular_name'         => $this->singular,
                'all_items'             => 'All ' . $this->plural,
                'add_new'               => 'Add ' . $this->plural,
                'add_new_item'          => 'Add ' .$this->singular,
                'edit_item'             => 'Edit ' .$this->singular,
                'view_item'             => 'View ' .$this->singular,
                'search_items'          => 'Search  ' .$this->singular,
                'not_found'             => 'No ' . $this->plural . ' find',
                'not_found_in_trash'    => 'No '  . $this->plural . ' find in trash',
            );

            $supports = array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            );

            $taxonomies = array(
            );

            $args = array(
                'labels'            => $labels,
                'public'            => true,
//                'publicly_queryable'=> true,
                'show_ui'           => true,
                'show_in_admin_bar' => true,
                'show_in_menu'      => true,
                'menu_icon'         => 'dashicons-chart-area',
                'can_export'        => true,
                'rewrite'           => array( 'slug' => 'booking' ),
                'has_archive'       => true,
                'query_var'         => true,
                'capability_type'   => 'booking',
//                'hierarchical'      => true,
                'capabilities'      => $capabilities,
                'map_meta_cap'      => true,
                'taxonomies'        => $taxonomies,
                'supports'          => $supports,
            );

            register_post_type( 'booking', $args );
            flush_rewrite_rules ();
        }

        /**
         * Register new taxonomy Delivery Method
         */
        public function registerTaxonomyDeliveryMethod(){
            
            $post_type = array( 'booking' );
            
            SC_Init::registerTaxonomy( 'Delivery method', 'Delivery methods', 'delivery_method', $post_type);

        }


        /**
         * Register new taxonomy Order Status
         */
        public function registerTaxonomyOrderStatus(){

            $post_type = array( 'booking' );

            SC_Init::registerTaxonomy( 'Order Status', 'Orders Status', 'order_status', $post_type);
            
        }


        /**
         * Register new term taxonomy
         */
        public function setTermTaxonomy(){

            SC_Init::registerTermTaxonomy( 'Pickup', 'delivery_method', 'Pickup', 'pickup' );
            SC_Init::registerTermTaxonomy( 'Mail delivery', 'delivery_method', 'Mail delivery', 'mail_delivery' );
            SC_Init::registerTermTaxonomy( 'Express delivery', 'delivery_method', 'Express delivery', 'express_delivery' );

            SC_Init::registerTermTaxonomy( 'Processed', 'order_status', 'Processed', 'processed' );
            SC_Init::registerTermTaxonomy( 'Sent', 'order_status', 'Sent', 'sent' );
            SC_Init::registerTermTaxonomy( 'Rejected', 'order_status', 'Rejected', 'rejected' );
            
        }


        /**
         * Remove term taxonomy
         */
        static function removeTermTaxonomy(){
            
        }

    }