<?php
    
    class SC_Product{

        public $singular = 'Product';
        public $plural = 'Products';

        public function __construct(){
            add_action( 'init', array( $this, 'newPostType' ) );
        }

        /**
         *  Register new post type
         */
        public function newPostType(){
            
            $capabilities = array(
            );
            
            $labels = array(
                'name'                  => $this->plural,
                'singular_name'         => $this->singular,
                'all_items'             => 'All ' . $this->plural,
                'add_new'               => 'Add ' . $this->plural,
                'add_new_item'          => 'Add ' .$this->singular,
                'edit_item'             => 'Edit ' .$this->singular,
                'view_item'             => 'View ' .$this->singular,
                'search_items'          => 'Search  ' .$this->singular,
                'not_found'             => 'No ' . $this->plural . ' find',
                'not_found_in_trash'    => 'No '  . $this->plural . ' find in trash',
            );
            
            $supports = array(
                'title', 
                'editor', 
                'excerpt', 
                'thumbnail',
            );
            
            $taxonomies = array(
                'category', 
                'post_tag',
            );

            $args = array(
                'labels'            => $labels,
                'public'            => true,
//                'publicly_queryable'=> true,
                'show_ui'           => true,
                'show_in_admin_bar' => true,
                'show_in_menu'      => true,
                'menu_icon'         => 'dashicons-store',
                'can_export'        => true,
                'rewrite'           => array( 'slug' => 'product' ),
                'has_archive'       => true,
                'query_var'         => true,
                'capability_type'   => 'product',
//                'hierarchical'      => true,
                'capabilities'      => $capabilities,
                'map_meta_cap'      => true,
                'taxonomies'        => $taxonomies,
                'supports'          => $supports,
            );

            register_post_type( 'product', $args );
            flush_rewrite_rules ();
        }
        
    }

