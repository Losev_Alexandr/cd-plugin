<?php
    /**
     * Plugin Name: Shop Codediller
     * Plugin URI: http://codediller.com/
     * Description: A plugin for creating and displaying shop opportunities 
     * Author: Codediller
     * Author URI: http://codediller.com/
     * Version: 0.0.1
     * License: GPLv2
    */
    
    //Exit if accessed directly
    if ( ! defined( 'ABSPATH' ) ){
        exit;
    }

    define( 'SHOP_CODDILER__PLUGIN_DIR',         plugin_dir_path( __FILE__ ) );
    define( 'SHOP_CODDILER__PLUGIN_URL',         plugin_dir_url( __FILE__ ) );
    define( 'SHOP_CODDILER__PLUGIN_FILE',        __FILE__ );
    

    require_once( SHOP_CODDILER__PLUGIN_DIR . 'includes/class.roles.php'   );
    require_once( SHOP_CODDILER__PLUGIN_DIR . 'includes/class.product.php' );
    require_once( SHOP_CODDILER__PLUGIN_DIR . 'includes/class.order.php'   );
    require_once( SHOP_CODDILER__PLUGIN_DIR . 'includes/class.init.php'    );
    require_once( SHOP_CODDILER__PLUGIN_DIR . 'class.buy.php'              );
    

    register_activation_hook(   SHOP_CODDILER__PLUGIN_FILE, array( 'SC_Init', 'onActivation' ) );
    register_deactivation_hook( SHOP_CODDILER__PLUGIN_FILE, array( 'SC_Init', 'onDeactivation' ) );
    register_uninstall_hook(    SHOP_CODDILER__PLUGIN_FILE, array( 'SC_Init', 'onUninstall' ) );
    
    SC_Init::init();

    
    