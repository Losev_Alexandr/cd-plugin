<?php 
    class SC_Buy {
        
        /**
         * Get all products
         * 
         * @return array $products
         */
        static function getAllProducts(){
            $args = array(
                'post_type'     => 'product',
                'numberposts'   => '-1',
            );
            
            $products = get_posts($args);
            return $products;
        }


        /**
         * Get products array and return only headers
         * 
         * @param array $products
         * @return array $products_header
         */
        static function getHeaderProduct($products = null){
            $products_headers = array();
            if($products == null){
                $products = self::getAllProducts();
            }
            foreach ($products as $product){
                array_push($products_headers, $product->post_title);
            }
            return $products_headers;
        }


        /**
         * Get names all delivery methods
         * 
         * @return array
         */
        static function getDeliveryMethods(){
            $args = array(
                'taxonomy'   => 'delivery_method',
                'hide_empty' => 'false',
                'fields'     =>'names',
                'get'        => 'all',
            );
            
            return get_terms( $args );
        }

        /**
         * Add ajax code to single-product page
         */
        static function registerScriptForAjax(){
            wp_enqueue_script( 'ajax', SHOP_CODDILER__PLUGIN_URL . 'js/sc_ajax.js' );
            wp_localize_script( 'ajax', 'shopCodediller',
                array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                )
            );
        }


        /**
         * Save orders
         */
        static function setOrderByAjax(){
            
            $user_data = array(
                'first_name'        => $_POST['first_name'],
                'last_name'         => $_POST['last_name'],
                'email'             => $_POST['email'],
                'product_name'      => $_POST['product_name'],
                'delivery_method'   => $_POST['delivery_method'],
            );
            
            // Check for completed fields
            foreach ($user_data as $key => $data ){
                if( empty($data) ){
                    echo 1002;
                    wp_die();
                }
            }
            
            //Get number last order and check whether he is first
            $last_order = get_posts("post_type=booking&numberposts=1");
            $last_order = $last_order[0]->ID;
            if(empty($last_order)){
                $order_number = 1;
            }else{
                $order_number = ++$last_order;
            }
            
            // Making order data
            $post_data = array(
                'post_title' => 'Order #' . $order_number,
                'post_content' => self::orderContent(
                    $order_number, 
                    $user_data['product_name'],
                    $user_data['first_name'],
                    $user_data['last_name'],
                    $user_data['email']
                ),
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type' => 'booking',
                'post_name' => 'order-' . $order_number,
                'tax_input' => array(
                    'delivery_method' => array( $user_data['delivery_method'] ),
                    'order_status' => array( 'processed' ),
                ),
            );
            
            $send_order = wp_insert_post($post_data);
            
            if($send_order){
                echo 1004;
                wp_die();
            }
            
            echo 1003;
            wp_die();
            
        }

        /**
         * Making order content
         * 
         * @param $order_number
         * @param string $product_name
         * @param string $first_name
         * @param string $last_name
         * @param string $email
         * @return string $post_content
         */
        static function orderContent( $order_number, $product_name, $first_name, $last_name, $email ){
            
            $post_content  = '<h2><span style="font-weight: 300; font-size: 24px">Order #'. $order_number .'</span></h2>';
            $post_content .= '<br />';
            $post_content .= '<p><span style="font-weight: 700">First name: </span>'. $first_name .'</p>';
            $post_content .= '<p><span style="font-weight: 700">Last name: </span>'. $last_name .'</p>';
            $post_content .= '<p><span style="font-weight: 700">Email: </span>'. $email .'</p>';
            $post_content .= '<p><span style="font-weight: 700">Product: </span>'. $product_name .'</p>';
            $post_content .= '<br />';
            $post_content .= '<p><span style="font-weight: 700">Date order: </span>'. current_time('d m Y H:i') .'</p>';
            return $post_content;

        }
    }