<?php get_header(); ?>
    
    <div class="container archive-page">
        <div class="row">
            <?php
                if ( have_posts() ) :

                    /* Start the Loop */
                    while ( have_posts() ) : the_post(); ?>

                        <article class="-col-4 post post-<?php the_ID(); ?>">
                            <div class="post-in-archive">
                                <div class="proj-gallery">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                </div>
                                <div>
                                    <h6 class="post-header"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                                    <div class="post-description">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <div class="button-buy">
                                    <a href="<?php the_permalink(); ?>"><?php echo __('Buy') ?></a>
                                </div>
                            </div>
                        </article>

                    <?php endwhile;

                else :
                    
                endif;
            ?>
        </div>
    </div>
    
<?php get_footer(); ?>