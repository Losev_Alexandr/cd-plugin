<?php get_header(); ?>
    
    <?php
        $current_user = wp_get_current_user();
        $products_headers =  SC_Buy::getHeaderProduct();
        $delivery_methods = SC_Buy::getDeliveryMethods();
//    var_dump($user_data);
    ?>
    
    <div class="single-product-post container">
        <?php
            if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post(); ?>

                    <article class="post post-<?php the_ID(); ?>">
                        <div class="row">
                            <div class="-col-6 post-in-archive">
                                <div class="proj-gallery">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <div>
                                    <div class="post-description">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="-col-6">
                                <div class="buy-product">
                                    <h1 class="post-header"><?php the_title(); ?></h1>
                                    <div class="button-buy">
                                        <a class="open-popup"><?php echo __('Buy') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <div class="single-product-popup">
                        <div class="button-close"></div>
                        <div class="vertical-center">
                            <div class="popup-buy">
                                <div class="popup-form">
                                    <div class="popup-header"><?php echo __('Order form') ?></div>
                                    <form type="POST" action="" class="send-order">
                                        <input type="text" value="<?php echo  $current_user->user_firstname; ?>" placeholder="First name" name="first_name">
                                        <input type="text" value="<?php echo  $current_user->user_lastname; ?>" placeholder="Last name" name="last_name">
                                        <input type="email" value="<?php echo $current_user->user_email; ?>" placeholder="Email" name="email">
                                        <select name="product_name">
                                            <?php foreach ($products_headers as $product_header ) : ?>
                                                <option value="<?php echo $product_header ?>"><?php echo $product_header ?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <select name="delivery_method">
                                            <?php foreach ($delivery_methods as $delivery_method ) : ?>
                                                <option value="<?php echo $delivery_method ?>"><?php echo $delivery_method ?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="submit" value="<?php echo __('Submit'); ?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ajax-answer">
                        <div class="button-close"></div>
                        <div class="description-error"></div>
                    </div>

                <?php endwhile;

            else :

            endif;
        ?>
    </div>
    
<?php get_footer(); ?>