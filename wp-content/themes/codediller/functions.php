<?php
    
    function style_resources(){
        wp_enqueue_style('style', get_stylesheet_uri());
    }
    add_action('wp_enqueue_scripts', 'style_resources');

    function script_resources(){
        wp_enqueue_script('jquery');
    }
    add_action( 'wp_enqueue_scripts', 'script_resources' );
    
    // Navigation menu
    register_nav_menus(array(
        'primary'   => __('Primary Menu'),
        'footer'    => __('Footer Menu'),
    ));

    add_theme_support( 'post-thumbnails' );