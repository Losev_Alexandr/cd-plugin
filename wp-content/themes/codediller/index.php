<?php get_header(); ?>
    
    <div class="container archive-page">
        <div class="row">
            <?php
                if ( have_posts() ) :

                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        //            get_template_part( 'template-parts/post/content', get_post_format() );
                        get_template_part( 'template-parts/product');

                    endwhile;

                else :

                    //        get_template_part( 'template-parts/post/content', 'none' );

                endif;
            ?>
        </div>
    </div>
<?php get_footer(); ?>