<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php bloginfo('name'); ?></title>
        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>

    <!-- Site header-->
    <header class="site-header">
        <div class="container clearfix">
            <a href="<?php echo home_url(); ?>" class="site-logo">
                <h2><?php bloginfo( 'name' ); ?></h2>
                <h4><?php bloginfo( 'description' ); ?></h4>
            </a>
            <nav class="site-navigation">
                <?php
                    $args = array(
                        'theme_location' => 'primary'
                    );
                ?>
                <?php wp_nav_menu($args); ?>
            </nav>
        </div>
    </header><!-- /Site header-->