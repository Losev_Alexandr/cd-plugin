<article class="-col-4 post post-<?php the_ID(); ?>">
   <div class="post-in-archive">
       <div class="proj-gallery">
           <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
       </div>
       <div>
           <h6 class="post-header"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
           <div class="post-description">
               <?php the_content(); ?>
           </div>
       </div>
   </div>
</article>